package br.com.integracao;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "NewDataSet")
public class RealizarConsultaSqlAuthReponseCustom {

	@XmlElement(name="Resultado")
	private Restultado Resultado;

	public Restultado getResultado() {
		return Resultado;
	}

	public void setResultado(Restultado resultado) {
		Resultado = resultado;
	}

	@Override
	public String toString() {
		return "RealizarConsultaSqlAuthReponseCustom [Resultado=" + Resultado + "]";
	}

}