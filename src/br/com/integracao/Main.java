package br.com.integracao;

import br.com.totvs.www.br.RealizarConsultaSQLAuth;

public class Main {

	public static void main(String[] args) {

		RealizarConsultaSQLAuth consulta = new RealizarConsultaSQLAuth(
				"GLOBAL_105", // cod sentenca
				0, // cod coligada
				"V", // cod Aplicacao
				"fluig", // usuario
				"fluig", // senha
				"email=edno.farias@manserv.com.br" // parametros
				);

		/**
		 * Formas de implementa��o 1� Op��o
		 */
		RealizarConsultaSqlAuthReponseCustom resultado = IntegracaoTotvs.consultaStatic(consulta);

		System.out.println(resultado.getResultado().getCODUT());
		System.out.println(resultado.getResultado().getNOMEUT());
		System.out.println(resultado);
		
		/**
		 * Formas de implementa��o 2� Op��o
		 */
		
		IntegracaoTotvs integracaoTotvs = new IntegracaoTotvs();
		resultado = integracaoTotvs.consultar(consulta);
		
		System.out.println(resultado.getResultado().getCODUT());
		System.out.println(resultado.getResultado().getNOMEUT());
		System.out.println(resultado);
	}
}