package br.com.integracao;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Resultado")
public class Restultado {

	private String CODEMPRESA;
	private String NOMEEMPRESA;
	private String NOMEFUNC;
	private String NOMEFUNCAO;
	private String CODUT;
	private String NOMEUT;

	public String getCODEMPRESA() {
		return CODEMPRESA;
	}

	public void setCODEMPRESA(String cODEMPRESA) {
		CODEMPRESA = cODEMPRESA;
	}

	public String getNOMEEMPRESA() {
		return NOMEEMPRESA;
	}

	public void setNOMEEMPRESA(String nOMEEMPRESA) {
		NOMEEMPRESA = nOMEEMPRESA;
	}

	public String getNOMEFUNC() {
		return NOMEFUNC;
	}

	public void setNOMEFUNC(String nOMEFUNC) {
		NOMEFUNC = nOMEFUNC;
	}

	public String getNOMEFUNCAO() {
		return NOMEFUNCAO;
	}

	public void setNOMEFUNCAO(String nOMEFUNCAO) {
		NOMEFUNCAO = nOMEFUNCAO;
	}

	public String getCODUT() {
		return CODUT;
	}

	public void setCODUT(String cODUT) {
		CODUT = cODUT;
	}

	public String getNOMEUT() {
		return NOMEUT;
	}

	public void setNOMEUT(String nOMEUT) {
		NOMEUT = nOMEUT;
	}

	@Override
	public String toString() {
		return "RealizarConsultaSqlAuthReponseCustom [CODEMPRESA=" + CODEMPRESA + ", NOMEEMPRESA=" + NOMEEMPRESA
				+ ", NOMEFUNC=" + NOMEFUNC + ", NOMEFUNCAO=" + NOMEFUNCAO + ", CODUT=" + CODUT + ", NOMEUT=" + NOMEUT
				+ "]";
	}

}
