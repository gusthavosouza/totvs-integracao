package br.com.integracao;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import br.com.totvs.www.br.RealizarConsultaSQLAuth;
import br.com.totvs.www.br.WsConsultaSQLSoapProxy;

public class IntegracaoTotvs {

	private static JAXBContext jaxbContext;
	private static Unmarshaller jaxbUnmarshaller;
	
	static {
		load();
	}
	
	private static void load() {
		try {
			jaxbContext = JAXBContext.newInstance(RealizarConsultaSqlAuthReponseCustom.class);
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			System.out.println("Ocorreu um erro ao inicialiar o componente de XML -> Object");
			e.printStackTrace();
		}
	}
	
	public RealizarConsultaSqlAuthReponseCustom consultar(RealizarConsultaSQLAuth consultaSQlAuth) {
		WsConsultaSQLSoapProxy ws = null;
		StringReader strReader = null;
		
		try {

			ws = new WsConsultaSQLSoapProxy();

			String response = ws.realizarConsultaSQLAuth(
					consultaSQlAuth.getCodSentenca(),
					consultaSQlAuth.getCodColigada(),
					consultaSQlAuth.getCodAplicacao(),
					consultaSQlAuth.getUsuario(),
					consultaSQlAuth.getSenha(),
					consultaSQlAuth.getParameters());
			
			strReader = new StringReader(response);
			
			RealizarConsultaSqlAuthReponseCustom consultaReponse = (RealizarConsultaSqlAuthReponseCustom) jaxbUnmarshaller.unmarshal(strReader);
			
			return consultaReponse;
		} catch (Exception e) {
			System.out.println("Falha ao consultar a API SOAP TOTVS");
			e.printStackTrace();
		} finally {
			try{strReader.close();} catch (Exception e) {}
		}
		return null;
	}
	
	public static RealizarConsultaSqlAuthReponseCustom consultaStatic(RealizarConsultaSQLAuth consultaSQlAuth) {
		
		WsConsultaSQLSoapProxy ws = null;
		StringReader strReader = null;
		
		try {

			ws = new WsConsultaSQLSoapProxy();

			String response = ws.realizarConsultaSQLAuth(
					consultaSQlAuth.getCodSentenca(),
					consultaSQlAuth.getCodColigada(),
					consultaSQlAuth.getCodAplicacao(),
					consultaSQlAuth.getUsuario(),
					consultaSQlAuth.getSenha(),
					consultaSQlAuth.getParameters());
			
			strReader = new StringReader(response);
			
			RealizarConsultaSqlAuthReponseCustom consultaReponse = (RealizarConsultaSqlAuthReponseCustom) jaxbUnmarshaller.unmarshal(strReader);
			
//			Restultado resultado = consultaReponse.getResultado();
			
			return consultaReponse;
		} catch (Exception e) {
			System.out.println("Falha ao consultar a API SOAP TOTVS");
			e.printStackTrace();
		} finally {
			try{strReader.close();} catch (Exception e) {}
		}
		return null;
	}
	
}
