/**
 * RealizarConsultaSQLDataTableAuthResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.totvs.www.br;

public class RealizarConsultaSQLDataTableAuthResponse  implements java.io.Serializable {
    private br.com.totvs.www.br.RealizarConsultaSQLDataTableAuthResponseRealizarConsultaSQLDataTableAuthResult realizarConsultaSQLDataTableAuthResult;

    public RealizarConsultaSQLDataTableAuthResponse() {
    }

    public RealizarConsultaSQLDataTableAuthResponse(
           br.com.totvs.www.br.RealizarConsultaSQLDataTableAuthResponseRealizarConsultaSQLDataTableAuthResult realizarConsultaSQLDataTableAuthResult) {
           this.realizarConsultaSQLDataTableAuthResult = realizarConsultaSQLDataTableAuthResult;
    }


    /**
     * Gets the realizarConsultaSQLDataTableAuthResult value for this RealizarConsultaSQLDataTableAuthResponse.
     * 
     * @return realizarConsultaSQLDataTableAuthResult
     */
    public br.com.totvs.www.br.RealizarConsultaSQLDataTableAuthResponseRealizarConsultaSQLDataTableAuthResult getRealizarConsultaSQLDataTableAuthResult() {
        return realizarConsultaSQLDataTableAuthResult;
    }


    /**
     * Sets the realizarConsultaSQLDataTableAuthResult value for this RealizarConsultaSQLDataTableAuthResponse.
     * 
     * @param realizarConsultaSQLDataTableAuthResult
     */
    public void setRealizarConsultaSQLDataTableAuthResult(br.com.totvs.www.br.RealizarConsultaSQLDataTableAuthResponseRealizarConsultaSQLDataTableAuthResult realizarConsultaSQLDataTableAuthResult) {
        this.realizarConsultaSQLDataTableAuthResult = realizarConsultaSQLDataTableAuthResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RealizarConsultaSQLDataTableAuthResponse)) return false;
        RealizarConsultaSQLDataTableAuthResponse other = (RealizarConsultaSQLDataTableAuthResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.realizarConsultaSQLDataTableAuthResult==null && other.getRealizarConsultaSQLDataTableAuthResult()==null) || 
             (this.realizarConsultaSQLDataTableAuthResult!=null &&
              this.realizarConsultaSQLDataTableAuthResult.equals(other.getRealizarConsultaSQLDataTableAuthResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRealizarConsultaSQLDataTableAuthResult() != null) {
            _hashCode += getRealizarConsultaSQLDataTableAuthResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RealizarConsultaSQLDataTableAuthResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", ">RealizarConsultaSQLDataTableAuthResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("realizarConsultaSQLDataTableAuthResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "RealizarConsultaSQLDataTableAuthResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", ">>RealizarConsultaSQLDataTableAuthResponse>RealizarConsultaSQLDataTableAuthResult"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
