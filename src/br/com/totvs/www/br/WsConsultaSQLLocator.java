/**
 * WsConsultaSQLLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.totvs.www.br;

public class WsConsultaSQLLocator extends org.apache.axis.client.Service implements br.com.totvs.www.br.WsConsultaSQL {

/**
 * Este serviço disponibiliza métodos para integração com o TOTVS
 * Educacional. 
 *               Todos os WebMethods existentes neste serviço utilizam
 * autenticação SOAP.
 */

    public WsConsultaSQLLocator() {
    }


    public WsConsultaSQLLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WsConsultaSQLLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for wsConsultaSQLSoap
    private java.lang.String wsConsultaSQLSoap_address = "http://fluigtbc.manserv.com.br:81/TOTVSBusinessConnect/wsConsultaSQL.asmx";

    public java.lang.String getwsConsultaSQLSoapAddress() {
        return wsConsultaSQLSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String wsConsultaSQLSoapWSDDServiceName = "wsConsultaSQLSoap";

    public java.lang.String getwsConsultaSQLSoapWSDDServiceName() {
        return wsConsultaSQLSoapWSDDServiceName;
    }

    public void setwsConsultaSQLSoapWSDDServiceName(java.lang.String name) {
        wsConsultaSQLSoapWSDDServiceName = name;
    }

    public br.com.totvs.www.br.WsConsultaSQLSoap getwsConsultaSQLSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(wsConsultaSQLSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getwsConsultaSQLSoap(endpoint);
    }

    public br.com.totvs.www.br.WsConsultaSQLSoap getwsConsultaSQLSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.totvs.www.br.WsConsultaSQLSoapStub _stub = new br.com.totvs.www.br.WsConsultaSQLSoapStub(portAddress, this);
            _stub.setPortName(getwsConsultaSQLSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setwsConsultaSQLSoapEndpointAddress(java.lang.String address) {
        wsConsultaSQLSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.totvs.www.br.WsConsultaSQLSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.totvs.www.br.WsConsultaSQLSoapStub _stub = new br.com.totvs.www.br.WsConsultaSQLSoapStub(new java.net.URL(wsConsultaSQLSoap_address), this);
                _stub.setPortName(getwsConsultaSQLSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("wsConsultaSQLSoap".equals(inputPortName)) {
            return getwsConsultaSQLSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "wsConsultaSQL");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "wsConsultaSQLSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("wsConsultaSQLSoap".equals(portName)) {
            setwsConsultaSQLSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
