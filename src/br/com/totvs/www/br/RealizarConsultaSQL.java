/**
 * RealizarConsultaSQL.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.totvs.www.br;

public class RealizarConsultaSQL  implements java.io.Serializable {
    private java.lang.String codSentenca;

    private int codColigada;

    private java.lang.String codAplicacao;

    private java.lang.String codUsuario;

    private java.lang.String parameters;

    public RealizarConsultaSQL() {
    }

    public RealizarConsultaSQL(
           java.lang.String codSentenca,
           int codColigada,
           java.lang.String codAplicacao,
           java.lang.String codUsuario,
           java.lang.String parameters) {
           this.codSentenca = codSentenca;
           this.codColigada = codColigada;
           this.codAplicacao = codAplicacao;
           this.codUsuario = codUsuario;
           this.parameters = parameters;
    }


    /**
     * Gets the codSentenca value for this RealizarConsultaSQL.
     * 
     * @return codSentenca
     */
    public java.lang.String getCodSentenca() {
        return codSentenca;
    }


    /**
     * Sets the codSentenca value for this RealizarConsultaSQL.
     * 
     * @param codSentenca
     */
    public void setCodSentenca(java.lang.String codSentenca) {
        this.codSentenca = codSentenca;
    }


    /**
     * Gets the codColigada value for this RealizarConsultaSQL.
     * 
     * @return codColigada
     */
    public int getCodColigada() {
        return codColigada;
    }


    /**
     * Sets the codColigada value for this RealizarConsultaSQL.
     * 
     * @param codColigada
     */
    public void setCodColigada(int codColigada) {
        this.codColigada = codColigada;
    }


    /**
     * Gets the codAplicacao value for this RealizarConsultaSQL.
     * 
     * @return codAplicacao
     */
    public java.lang.String getCodAplicacao() {
        return codAplicacao;
    }


    /**
     * Sets the codAplicacao value for this RealizarConsultaSQL.
     * 
     * @param codAplicacao
     */
    public void setCodAplicacao(java.lang.String codAplicacao) {
        this.codAplicacao = codAplicacao;
    }


    /**
     * Gets the codUsuario value for this RealizarConsultaSQL.
     * 
     * @return codUsuario
     */
    public java.lang.String getCodUsuario() {
        return codUsuario;
    }


    /**
     * Sets the codUsuario value for this RealizarConsultaSQL.
     * 
     * @param codUsuario
     */
    public void setCodUsuario(java.lang.String codUsuario) {
        this.codUsuario = codUsuario;
    }


    /**
     * Gets the parameters value for this RealizarConsultaSQL.
     * 
     * @return parameters
     */
    public java.lang.String getParameters() {
        return parameters;
    }


    /**
     * Sets the parameters value for this RealizarConsultaSQL.
     * 
     * @param parameters
     */
    public void setParameters(java.lang.String parameters) {
        this.parameters = parameters;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RealizarConsultaSQL)) return false;
        RealizarConsultaSQL other = (RealizarConsultaSQL) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codSentenca==null && other.getCodSentenca()==null) || 
             (this.codSentenca!=null &&
              this.codSentenca.equals(other.getCodSentenca()))) &&
            this.codColigada == other.getCodColigada() &&
            ((this.codAplicacao==null && other.getCodAplicacao()==null) || 
             (this.codAplicacao!=null &&
              this.codAplicacao.equals(other.getCodAplicacao()))) &&
            ((this.codUsuario==null && other.getCodUsuario()==null) || 
             (this.codUsuario!=null &&
              this.codUsuario.equals(other.getCodUsuario()))) &&
            ((this.parameters==null && other.getParameters()==null) || 
             (this.parameters!=null &&
              this.parameters.equals(other.getParameters())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodSentenca() != null) {
            _hashCode += getCodSentenca().hashCode();
        }
        _hashCode += getCodColigada();
        if (getCodAplicacao() != null) {
            _hashCode += getCodAplicacao().hashCode();
        }
        if (getCodUsuario() != null) {
            _hashCode += getCodUsuario().hashCode();
        }
        if (getParameters() != null) {
            _hashCode += getParameters().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RealizarConsultaSQL.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", ">RealizarConsultaSQL"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codSentenca");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "codSentenca"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codColigada");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "codColigada"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codAplicacao");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "codAplicacao"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codUsuario");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "codUsuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameters");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "parameters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
