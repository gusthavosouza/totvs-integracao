/**
 * WsConsultaSQL.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.totvs.www.br;

public interface WsConsultaSQL extends javax.xml.rpc.Service {

/**
 * Este serviço disponibiliza métodos para integração com o TOTVS
 * Educacional. 
 *               Todos os WebMethods existentes neste serviço utilizam
 * autenticação SOAP.
 */
    public java.lang.String getwsConsultaSQLSoapAddress();

    public br.com.totvs.www.br.WsConsultaSQLSoap getwsConsultaSQLSoap() throws javax.xml.rpc.ServiceException;

    public br.com.totvs.www.br.WsConsultaSQLSoap getwsConsultaSQLSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
