/**
 * WsConsultaSQLSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.totvs.www.br;

public interface WsConsultaSQLSoap extends java.rmi.Remote {

    /**
     * Autentica o usuário no ambiente RM. 
     *           O usuário e senha devem ser informados por parâmetro.
     */
    public java.lang.String autenticaAcessoAuth(java.lang.String usuario, java.lang.String senha) throws java.rmi.RemoteException;

    /**
     * Autentica o acesso do usuário no ambiente RM. O usuário e 
     *         senha terá que ser passado via SOAP, criando um token para
     * isto.
     */
    public java.lang.String autenticaAcesso() throws java.rmi.RemoteException;

    /**
     * Realizar Consulta SQL
     */
    public br.com.totvs.www.br.RealizarConsultaSQLDataTableResponseRealizarConsultaSQLDataTableResult realizarConsultaSQLDataTable(java.lang.String codSentenca, int codColigada, java.lang.String codSistema, java.lang.String codUsuario, java.lang.String parameters) throws java.rmi.RemoteException;

    /**
     * Realizar Consulta SQL
     */
    public br.com.totvs.www.br.RealizarConsultaSQLDataTableAuthResponseRealizarConsultaSQLDataTableAuthResult realizarConsultaSQLDataTableAuth(java.lang.String codSentenca, int codColigada, java.lang.String codAplicacao, java.lang.String usuario, java.lang.String senha, java.lang.String parameters) throws java.rmi.RemoteException;

    /**
     * Realizar Consulta SQL
     */
    public java.lang.String realizarConsultaSQL(java.lang.String codSentenca, int codColigada, java.lang.String codAplicacao, java.lang.String codUsuario, java.lang.String parameters) throws java.rmi.RemoteException;

    /**
     * Realizar Consulta SQL
     */
    public java.lang.String realizarConsultaSQLAuth(java.lang.String codSentenca, int codColigada, java.lang.String codAplicacao, java.lang.String usuario, java.lang.String senha, java.lang.String parameters) throws java.rmi.RemoteException;
}
