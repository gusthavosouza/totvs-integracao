/**
 * RealizarConsultaSQLAuthResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.totvs.www.br;

public class RealizarConsultaSQLAuthResponse  implements java.io.Serializable {
    private java.lang.String realizarConsultaSQLAuthResult;

    public RealizarConsultaSQLAuthResponse() {
    }

    public RealizarConsultaSQLAuthResponse(
           java.lang.String realizarConsultaSQLAuthResult) {
           this.realizarConsultaSQLAuthResult = realizarConsultaSQLAuthResult;
    }


    /**
     * Gets the realizarConsultaSQLAuthResult value for this RealizarConsultaSQLAuthResponse.
     * 
     * @return realizarConsultaSQLAuthResult
     */
    public java.lang.String getRealizarConsultaSQLAuthResult() {
        return realizarConsultaSQLAuthResult;
    }


    /**
     * Sets the realizarConsultaSQLAuthResult value for this RealizarConsultaSQLAuthResponse.
     * 
     * @param realizarConsultaSQLAuthResult
     */
    public void setRealizarConsultaSQLAuthResult(java.lang.String realizarConsultaSQLAuthResult) {
        this.realizarConsultaSQLAuthResult = realizarConsultaSQLAuthResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RealizarConsultaSQLAuthResponse)) return false;
        RealizarConsultaSQLAuthResponse other = (RealizarConsultaSQLAuthResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.realizarConsultaSQLAuthResult==null && other.getRealizarConsultaSQLAuthResult()==null) || 
             (this.realizarConsultaSQLAuthResult!=null &&
              this.realizarConsultaSQLAuthResult.equals(other.getRealizarConsultaSQLAuthResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRealizarConsultaSQLAuthResult() != null) {
            _hashCode += getRealizarConsultaSQLAuthResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RealizarConsultaSQLAuthResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", ">RealizarConsultaSQLAuthResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("realizarConsultaSQLAuthResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "RealizarConsultaSQLAuthResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
