/**
 * RealizarConsultaSQLResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.totvs.www.br;

public class RealizarConsultaSQLResponse  implements java.io.Serializable {
    private java.lang.String realizarConsultaSQLResult;

    public RealizarConsultaSQLResponse() {
    }

    public RealizarConsultaSQLResponse(
           java.lang.String realizarConsultaSQLResult) {
           this.realizarConsultaSQLResult = realizarConsultaSQLResult;
    }


    /**
     * Gets the realizarConsultaSQLResult value for this RealizarConsultaSQLResponse.
     * 
     * @return realizarConsultaSQLResult
     */
    public java.lang.String getRealizarConsultaSQLResult() {
        return realizarConsultaSQLResult;
    }


    /**
     * Sets the realizarConsultaSQLResult value for this RealizarConsultaSQLResponse.
     * 
     * @param realizarConsultaSQLResult
     */
    public void setRealizarConsultaSQLResult(java.lang.String realizarConsultaSQLResult) {
        this.realizarConsultaSQLResult = realizarConsultaSQLResult;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RealizarConsultaSQLResponse)) return false;
        RealizarConsultaSQLResponse other = (RealizarConsultaSQLResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.realizarConsultaSQLResult==null && other.getRealizarConsultaSQLResult()==null) || 
             (this.realizarConsultaSQLResult!=null &&
              this.realizarConsultaSQLResult.equals(other.getRealizarConsultaSQLResult())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRealizarConsultaSQLResult() != null) {
            _hashCode += getRealizarConsultaSQLResult().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RealizarConsultaSQLResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", ">RealizarConsultaSQLResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("realizarConsultaSQLResult");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.totvs.com.br/br/", "RealizarConsultaSQLResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
