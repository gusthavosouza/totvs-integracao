package br.com.totvs.www.br;

public class WsConsultaSQLSoapProxy implements br.com.totvs.www.br.WsConsultaSQLSoap {
  private String _endpoint = null;
  private br.com.totvs.www.br.WsConsultaSQLSoap wsConsultaSQLSoap = null;
  
  public WsConsultaSQLSoapProxy() {
    _initWsConsultaSQLSoapProxy();
  }
  
  public WsConsultaSQLSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initWsConsultaSQLSoapProxy();
  }
  
  private void _initWsConsultaSQLSoapProxy() {
    try {
      wsConsultaSQLSoap = (new br.com.totvs.www.br.WsConsultaSQLLocator()).getwsConsultaSQLSoap();
      if (wsConsultaSQLSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wsConsultaSQLSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wsConsultaSQLSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wsConsultaSQLSoap != null)
      ((javax.xml.rpc.Stub)wsConsultaSQLSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.totvs.www.br.WsConsultaSQLSoap getWsConsultaSQLSoap() {
    if (wsConsultaSQLSoap == null)
      _initWsConsultaSQLSoapProxy();
    return wsConsultaSQLSoap;
  }
  
  public java.lang.String autenticaAcessoAuth(java.lang.String usuario, java.lang.String senha) throws java.rmi.RemoteException{
    if (wsConsultaSQLSoap == null)
      _initWsConsultaSQLSoapProxy();
    return wsConsultaSQLSoap.autenticaAcessoAuth(usuario, senha);
  }
  
  public java.lang.String autenticaAcesso() throws java.rmi.RemoteException{
    if (wsConsultaSQLSoap == null)
      _initWsConsultaSQLSoapProxy();
    return wsConsultaSQLSoap.autenticaAcesso();
  }
  
  public br.com.totvs.www.br.RealizarConsultaSQLDataTableResponseRealizarConsultaSQLDataTableResult realizarConsultaSQLDataTable(java.lang.String codSentenca, int codColigada, java.lang.String codSistema, java.lang.String codUsuario, java.lang.String parameters) throws java.rmi.RemoteException{
    if (wsConsultaSQLSoap == null)
      _initWsConsultaSQLSoapProxy();
    return wsConsultaSQLSoap.realizarConsultaSQLDataTable(codSentenca, codColigada, codSistema, codUsuario, parameters);
  }
  
  public br.com.totvs.www.br.RealizarConsultaSQLDataTableAuthResponseRealizarConsultaSQLDataTableAuthResult realizarConsultaSQLDataTableAuth(java.lang.String codSentenca, int codColigada, java.lang.String codAplicacao, java.lang.String usuario, java.lang.String senha, java.lang.String parameters) throws java.rmi.RemoteException{
    if (wsConsultaSQLSoap == null)
      _initWsConsultaSQLSoapProxy();
    return wsConsultaSQLSoap.realizarConsultaSQLDataTableAuth(codSentenca, codColigada, codAplicacao, usuario, senha, parameters);
  }
  
  public java.lang.String realizarConsultaSQL(java.lang.String codSentenca, int codColigada, java.lang.String codAplicacao, java.lang.String codUsuario, java.lang.String parameters) throws java.rmi.RemoteException{
    if (wsConsultaSQLSoap == null)
      _initWsConsultaSQLSoapProxy();
    return wsConsultaSQLSoap.realizarConsultaSQL(codSentenca, codColigada, codAplicacao, codUsuario, parameters);
  }
  
  public java.lang.String realizarConsultaSQLAuth(java.lang.String codSentenca, int codColigada, java.lang.String codAplicacao, java.lang.String usuario, java.lang.String senha, java.lang.String parameters) throws java.rmi.RemoteException{
    if (wsConsultaSQLSoap == null)
      _initWsConsultaSQLSoapProxy();
    return wsConsultaSQLSoap.realizarConsultaSQLAuth(codSentenca, codColigada, codAplicacao, usuario, senha, parameters);
  }
  
  
}