# README #
# Integração Totvs
## Conteúdo do repositório
### JAR e Código fonte

## Exemplo de utilização
```
RealizarConsultaSQLAuth consulta = new RealizarConsultaSQLAuth(
				"GLOBAL_105", // cod sentenca
				0, // cod coligada
				"V", // cod Aplicacao
				"fluig", // usuario
				"fluig", // senha
				"email=edno.farias@manserv.com.br" // parametros
				);

		/**
		 * Formas de implementação 1º Opção
		 */
		RealizarConsultaSqlAuthReponseCustom resultado = IntegracaoTotvs.consultaStatic(consulta);

		System.out.println(resultado.getResultado().getCODUT());
		System.out.println(resultado.getResultado().getNOMEUT());
		System.out.println(resultado);
		
		/**
		 * Formas de implementação 2º Opção
		 */
		
		IntegracaoTotvs integracaoTotvs = new IntegracaoTotvs();
		resultado = integracaoTotvs.consultar(consulta);
		
		System.out.println(resultado.getResultado().getCODUT());
		System.out.println(resultado.getResultado().getNOMEUT());
		System.out.println(resultado);
```
Exemplo de utilização na classe Main do projeto.

## Caso de algum erro de versão do java. 

Verificar a versão do sysaid, para recompilar o jar na versão correta.

## Problemas na inicialização

No caso de problemas na inicialização do Sysaid outro algo do tipo, remover a classe Main do fonte e compilar o Jar.